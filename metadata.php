<?php
/**
 * @package       flowcat
 * @category      module
 * @author        Steffen Winde
 * @link          http://winde-ganzig.de/
 * @licenses      GNU GENERAL PUBLIC LICENSE. More info can be found in LICENSE file.
 * @copyright (C] OXID e-Sales, 2003-2017
 */

/**
 * Metadata version
 */
$sMetadataVersion = '2.1';

/**
 * Module information
 */
$aModule = [
    'id'           => 'flowcat',
    'title'       => [
        'de' => '.BEES | Flow-Theme Kategorie-Beschreibung',
        'en' => '.BEES | Flow-Theme Kategorie-Beschreibung',
    ],
    'description' => [
        'de' => 'Flow-Theme Kategorie-Beschreibung erweitern',
        'en' => 'Expand Flow-Theme Category description',
    ],
    'thumbnail'     => '',
    'version'       => '1.0.3',
    'author'        => 'Steffen Winde, Rafig Abbasov oxid-design.com ',
    'url'           => 'https://internetservice.winde-ganzig.de',
    'email'         => 'inserv@winde-ganzig.de',
    'extend'       => [
       \OxidEsales\Eshop\Application\Controller\Admin\CategoryText::class => \Swinde\FlowCat\Controller\Admin\CategoryText2::class,
    ],
    'events'       => [
        'onActivate'   => '\Swinde\FlowCat\Core\Events\CategoryText2Event::onActivate',
        'onDeactivate' => '\Swinde\FlowCat\Core\Events\CategoryText2Event::onDeactivate',
    ],

    'templates'   => [
        'category_text2.tpl' => 'swinde/flowcat/views/admin/tpl/category_text2.tpl',
    ],

    'blocks' => [
        [
            'template'=> 'page/list/list.tpl',
            'block'=>'page_list_listbody',
            'file'=>'views/blocks/page/list/category_text2_list.tpl'
        ]
    ],
];
