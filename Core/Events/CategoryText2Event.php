<?php
/**
 * This file is part of OXID eSales GDPR opt-in module.
 *
 * OXID eSales GDPR opt-in module is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OXID eSales GDPR opt-in module is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OXID eSales GDPR opt-in module.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @link      http://www.oxid-esales.com
 * @copyright (C) OXID eSales AG 2003-2018
 */

namespace Swinde\FlowCat\Core\Events;
use OxidEsales\Eshop\Core\Registry;

class CategoryText2Event {

    /**
     * Add additional fields:
     */
    public static function onActivate()
    {
        if( !self::_swCheckIfDatabaseFieldExists() ) {
            self::swAlterArticlesTable();
        }

    }

    public static function swAlterArticlesTable() {
        $oDb = \OxidEsales\Eshop\Core\DatabaseProvider::getDb();
        $oDb->Execute("ALTER TABLE `oxcategories` 
            ADD COLUMN `OXLONGDESCB` text NOT NULL COMMENT 'Long second description (multilanguage)', 
            ADD COLUMN `OXLONGDESCB_1` text NOT NULL COMMENT 'Long second description (multilanguage)', 
            ADD COLUMN `OXLONGDESCB_2` text NOT NULL COMMENT 'Long second description (multilanguage)';"
        );
        $oMetaData = oxNew(\OxidEsales\Eshop\Core\DbMetaDataHandler::class);
        $oMetaData->updateViews();
    }

    public static function clearTmp($sClearFolderPath = '')
    {
        $sFolderPath = self::_getFolderToClear($sClearFolderPath);
        $hDirHandler = opendir($sFolderPath);
        if (!empty($hDirHandler)) {
            while (false !== ($sFileName = readdir($hDirHandler))) {
                $sFilePath = $sFolderPath . DIRECTORY_SEPARATOR . $sFileName;
                self::_clear($sFileName, $sFilePath);
            }
            closedir($hDirHandler);
        }
        return true;
    }

    protected static function _getFolderToClear($sClearFolderPath = '')
    {
        $sTempFolderPath = (string) Registry::getConfig()->getConfigParam('sCompileDir');
        if (!empty($sClearFolderPath) and (strpos($sClearFolderPath, $sTempFolderPath) !== false)) {
            $sFolderPath = $sClearFolderPath;
        } else {
            $sFolderPath = $sTempFolderPath;
        }
        return $sFolderPath;
    }

    protected static function _clear($sFileName, $sFilePath)
    {
        if (!in_array($sFileName, ['.', '..', '.gitkeep', '.htaccess'])) {
            if (is_file($sFilePath)) {
                @unlink($sFilePath);
            } else {
                self::clearTmp($sFilePath);
            }
        }
    }
    protected static function _swCheckIfDatabaseFieldExists()
    {
        $blSwDatabaseFieldExists = false;

        $sTable = 'oxcategories';
        $sColumn = 'OXLONGDESCB';
        $oDbHandler = oxNew( "oxDbMetaDataHandler" );
        $blSwDatabaseFieldExists = $oDbHandler->tableExists( $sTable ) && $oDbHandler->fieldExists( $sColumn, $sTable );

        return $blSwDatabaseFieldExists;
    }
    public static function onDeactivate() {

    }

}